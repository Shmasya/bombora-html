/**
 * Section: jQuery
 */

$(function() {
	
	initNavigationUIModule();
	
	$(window).resize(function() {
		initNavigationUIModule();
	});
	
	initTabsUIModule();
	
	initSectionGeneralSlider();
	
	initSectionItemsCarousel();
	
	initSectionBigBannersSlider();
	
	initMobileNavigationToggle();
	
	initSearch();
	
	initPost2();
	
	$(window).resize(function() {
		initPost2();
	});
	
});

function initNavigationUIModule() {
	var $menuItemHasChildren = $('.ui.navigation .menu-item-has-children');
	
	$menuItemHasChildren.off();
	
	$menuItemHasChildren.click(function() {
		$(this).toggleClass('is-opened');
	});
}

function initTabsUIModule() {
	var $navigationTab = $('.navigation-tabs .navigation-tab');
	
	$navigationTab.click(function() {
		$(this).parent().children('.is-active').removeClass('is-active');
		$(this).addClass('is-active');
		var $tabsContent = $('.content-tabs[data-tabs-content="' + $(this).parent().data('tabs-content') + '"]');
		$tabsContent.children('.is-active').removeClass('is-active');
		$tabsContent.children('.content-tab').eq($(this).index()).addClass('is-active');
	});
}

function initSectionGeneralSlider() {
	$('.general-slider-slick').each(function() {
		$(this).slick({
			autoplay: true,
			appendArrows: $(this).parent().find('.general-slider-slick-arrows'),
			prevArrow: '<button class="prev"><i class="ion ion-ios-arrow-left"></i></button>',
			nextArrow: '<button class="next"><i class="ion ion-ios-arrow-right"></i></button>'
		});
	});
}

function initSectionItemsCarousel() {
	$('.items-carousel-slick').each(function() {
		$(this).slick({
			slidesToShow: 5,
			slidesToScroll: 5,
			autoplay: true,
			appendArrows: $(this).parent().find('.items-carousel-slick-arrows'),
			prevArrow: '<button class="prev"><i class="ion ion-ios-arrow-left"></i></button>',
			nextArrow: '<button class="next"><i class="ion ion-ios-arrow-right"></i></button>',
			responsive: [
				{
					breakpoint: 1199,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 4
					}
				},
				{
					breakpoint: 991,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});
	});
	
	$('.items-carousel-slick-2').each(function() {
		$(this).slick({
			slidesToShow: 3,
			slidesToScroll: 3,
			autoplay: true,
			appendArrows: $(this).parent().find('.items-carousel-slick-arrows'),
			prevArrow: '<button class="prev"><i class="ion ion-ios-arrow-left"></i></button>',
			nextArrow: '<button class="next"><i class="ion ion-ios-arrow-right"></i></button>',
			responsive: [
				{
					breakpoint: 991,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
					breakpoint: 575,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});
	});
	
	$('.items-carousel-slick-3').each(function() {
		$(this).slick({
			slidesToShow: 6,
			slidesToScroll: 6,
			autoplay: true,
			appendArrows: $(this).parent().find('.items-carousel-slick-arrows'),
			prevArrow: '<button class="prev"><i class="ion ion-ios-arrow-left"></i></button>',
			nextArrow: '<button class="next"><i class="ion ion-ios-arrow-right"></i></button>',
			responsive: [
				{
					breakpoint: 1199,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 4
					}
				},
				{
					breakpoint: 991,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});
	});
	
	$('.items-carousel-slick-4').each(function() {
		$(this).slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			draggable: false,
			swipe: false,
			touchMove: false,
			arrows: false,
			asNavFor: '.items-carousel-slick-5'
		});
	});
	
	$('.items-carousel-slick-5').each(function() {
		$(this).slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			autoplay: true,
			focusOnSelect: true,
			appendArrows: $(this).parent().find('.items-carousel-slick-arrows'),
			prevArrow: '<button class="prev"><i class="ion ion-ios-arrow-left"></i></button>',
			nextArrow: '<button class="next"><i class="ion ion-ios-arrow-right"></i></button>',
			asNavFor: '.items-carousel-slick-4'
		});
	});
	
	$('.items-carousel-slick-6').each(function() {
		$(this).slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			appendArrows: $(this).parent().find('.items-carousel-slick-arrows'),
			prevArrow: '<button class="prev"><i class="ion ion-ios-arrow-left"></i></button>',
			nextArrow: '<button class="next"><i class="ion ion-ios-arrow-right"></i></button>'
		});
	});
}

function initSectionBigBannersSlider() {
	$('.big-banners-slider-slick').each(function() {
		$(this).slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			focusOnSelect: true,
			autoplay: true,
			appendArrows: $(this).parent().find('.big-banners-slider-slick-arrows'),
			prevArrow: '<button class="prev"><i class="ion ion-ios-arrow-left"></i></button>',
			nextArrow: '<button class="next"><i class="ion ion-ios-arrow-right"></i></button>'
		});
	});
}

function initMobileNavigationToggle() {
	$('body').on('click', '[data-action="toggle-mobile-navigation"]', function(e) {
		$('section.section.mobile-navigation').toggleClass('is-shown');
		$('html').toggleClass('is-overlay');
	});
}

function initSearch() {
	$('body').on('click', '[data-action="toggle-header-search"]', function(e) {
		$('.section.header .search-form').toggleClass('is-shown');
	});
	
	$('body').on('click', function(e) {
		if (!$(e.target).closest('.search-form').length && !$(e.target).closest('.search').length) {
			$('.section.header .search-form').removeClass('is-shown');
		}
	});
	
	$('body').on('focus', '.header .search-form input', function() {
		$('.header .search-form').addClass('is-collapsed');
	});
	
	$('body').on('focusout', '.header .search-form input', function(e) {
		if ($(e.target).val().length) return false;
		$('.header .search-form').removeClass('is-collapsed');
	});
}

function initPost2() {
	if ($(window).width() >= 768) {
		$('.ui.post.style-full-2 .post-toggle').stick_in_parent({
			offset_top: 60
		});
	}
}

gumshoe.init({
	activeClass: 'is-active',
	offset: 0
});

var scroll = new SmoothScroll('a[href*="#"]');

/**
 * Bit Tooltip Cookie
 */

initBitTooltipCookie();

function initBitTooltipCookie() {
	if (!localStorage.getItem('cookie-notified')) {
		$('.cookie-tooltip').show();
	}
	
	$('[data-action="close-cookie-tooltip"]').click(function() {
		$('.cookie-tooltip').hide();
		localStorage.setItem('cookie-notified', true);
	});
}